+++
title = "LabVeillTech"
date = "2020-11-21"
author = "Léonard Michiels"
+++

# Atelier de veille technologique

Dans le cadre du cours de veille technologique dispensé par Marc Friedrich à l'HEIG-VD à Yverdon, 
j'ai mis en place ce petit blog pour regrouper les sujets qui m'intéressent et sur lesquels je veux rester à la page:

- Les phénomènes technologiques actuelles
- Les tips and tricks en matière de developpement
- Les usages UX actuels

Chacun de ces thèmes est abordés dans un article que l'on peut consulter ici.